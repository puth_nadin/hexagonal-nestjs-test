import { Module } from '@nestjs/common';
import { DomainModule } from 'src/domain/domain.module';
import { AppController } from './controller/app.controller';

@Module({
  imports: [DomainModule],
  controllers: [AppController],
})
export class ApplicationModule {}
