import { Module } from '@nestjs/common';
import { InfrastructureModule } from 'src/infrastructure/infrastructure.module';
import { APP_SERVICE } from './service/app.service';
import { AppService } from './service/impl/app.service';

@Module({
  imports: [InfrastructureModule],
  providers: [
    {
      provide: APP_SERVICE,
      useClass: AppService,
    },
  ],
  exports: [
    {
      provide: APP_SERVICE,
      useClass: AppService,
    },
  ],
})
export class DomainModule {}
