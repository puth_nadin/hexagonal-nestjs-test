export const MAILER_SERVICE = 'MAILER_SERVICE';

export interface IMailerService {
  send(): Promise<boolean>;
}
