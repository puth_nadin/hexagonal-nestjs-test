import { Inject, Injectable } from '@nestjs/common';
import {
  IMailerService,
  MAILER_SERVICE,
} from 'src/domain/adapter/mailer/mailer.service';
import { IAppService } from '../app.service';

@Injectable()
export class AppService implements IAppService {
  constructor(
    @Inject(MAILER_SERVICE)
    private readonly mailerService: IMailerService,
  ) {}

  async getHello(): Promise<string> {
    await this.mailerService.send();
    return Promise.resolve('Hello World!');
  }
}
