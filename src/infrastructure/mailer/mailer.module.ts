import { Module } from '@nestjs/common';
import { MAILER_SERVICE } from 'src/domain/adapter/mailer/mailer.service';
import { MailerService } from './mailer.service';

@Module({
  providers: [
    {
      provide: MAILER_SERVICE,
      useClass: MailerService,
    },
  ],
  exports: [
    {
      provide: MAILER_SERVICE,
      useClass: MailerService,
    },
  ],
})
export class MailerModule {}
