import { Injectable, Logger } from '@nestjs/common';
import { IMailerService } from 'src/domain/adapter/mailer/mailer.service';

@Injectable()
export class MailerService implements IMailerService {
  private readonly logger: Logger = new Logger(MailerService.name);

  send(): Promise<boolean> {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.logger.debug('Sending...');
        this.logger.debug('Sent!');
        resolve(true);
      }, 1000);
    });
  }
}
